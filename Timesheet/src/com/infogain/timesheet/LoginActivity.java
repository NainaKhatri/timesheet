package com.infogain.timesheet;

/*
 * 
 * Naina Khatri
 */
import java.security.DomainCombiner;

import com.infogain.timesheet.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.infogain.timesheet.R;

public class LoginActivity extends Activity{
	Spinner domainSpinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		// getActionBar().hide();

		domainSpinner = (Spinner) findViewById(R.id.doaminSpinner);

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.domain_array,
				android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		domainSpinner.setAdapter(adapter);
//		domainSpinner.setBackgroundColor(R.andr);
		domainSpinner.setOnItemSelectedListener(
	              new AdapterView.OnItemSelectedListener() {
	                  @Override
	                  public void onItemSelected(AdapterView<?> adapter, View view,
	                          int position, long id) {
	                		System.out.println("oinnnnn"+position);
	                		domainSpinner.setSelected(true);
	                		//domainSpinner.
	                		//domainSpinner.getSelectedItemPosition();
//	                    int position = spnr.getSelectedItemPosition();
//	                    Toast.makeText(getApplicationContext(),"You have selected "+celebrities[+position],Toast.LENGTH_LONG).show();
	                      // TODO Auto-generated method stub
	                  }
	                  @Override
	                  public void onNothingSelected(AdapterView<?> arg0) {
	                      // TODO Auto-generated method stub
	                  }
	              }
	          );
	}

	//
	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main, menu);
	// return true;
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id = item.getItemId();
	// if (id == R.id.action_settings) {
	// return true;
	// }
	// return super.onOptionsItemSelected(item);
	// }

	public void loginAction(View view) {
		// Intent intent = new Intent(this, ResideMenuActivity.class);
		// startActivity(intent);
		Intent intent = new Intent(this, PendingTimesheetActivity.class);
		startActivity(intent);

	}

//	@Override
//	public void onItemSelected(AdapterView<?> parent, View view, int position,
//			long id) {
//		System.out.println("oinnnnn"+position);
//		domainSpinner.setSelection(position-1);
//		
//		// TODO Auto-generated method stub
//	}
//
//	@Override
//	public void onNothingSelected(AdapterView<?> parent) {
//		// TODO Auto-generated method stub
//
//	}
}
