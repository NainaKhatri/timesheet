package com.infogain.timesheet.Dialogs;
/**
 * Developed by:- Divyanshu Srivastava
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.infogain.timesheet.R;

public class LoginMesDialog extends Dialog implements android.view.View.OnClickListener{

	private RelativeLayout relOkDialog;
	private String dialogName;
	private Context context;
	private TextView dialogBoldText, dialogText1, dialogText2;

	public LoginMesDialog(Context context , String dialogName ) {
		super(context);
		this.context = context;
		this.dialogName = dialogName;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_login_failed);

		initializeControls();
	}

	private void initializeControls() {

		relOkDialog = (RelativeLayout) findViewById(R.id.relOkDialog);
		relOkDialog.setOnClickListener(this);

		dialogBoldText = (TextView) findViewById(R.id.dialogBoldText);
		dialogText1 = (TextView) findViewById(R.id.dialogText1);

		if(dialogName.equals("NO_INTERNET"))
		{
			dialogBoldText.setText(R.string.no_internat_head);
			dialogText1.setText(R.string.no_internat_body);
		}
		else if(dialogName.equals("LOGIN_FAILED"))
		{
			dialogBoldText.setText(R.string.login_failed);
			dialogText1.setText(R.string.email_or_password_was_incorrect_);
		}
		else if(dialogName.equals("REGISTERTRATION_SUCCESSFUL"))
		{
			dialogBoldText.setText(R.string.registered);
			dialogText1.setText(R.string.register_text);
		}
		else if(dialogName.equals("PASSWORDS_MISMATCH"))
		{
			dialogBoldText.setText(R.string.password_mismatch_head);
			dialogText1.setText(R.string.password_mismatch_text);
		}
		else if(dialogName.equals("REGISTRATION_BLANK"))
		{
			dialogBoldText.setText(R.string.required_fields);
			dialogText1.setText(R.string.no_field_blank);
		}
		else if(dialogName.equals("NO_VALID_EMAIL"))
		{
			dialogText1.setText(R.string.no_valid_email);
		}
		else if(dialogName.equals("REGISTRATION_FAILED"))
		{
			dialogBoldText.setText(R.string.failed);
			dialogText1.setText(R.string.failed_register);
		}
		else if(dialogName.equals("PASSWORD_RESET"))
		{
			dialogBoldText.setVisibility(View.GONE);
			dialogText1.setText(R.string.password_reset);
		}
		else if(dialogName.equals("PASSWORD_RESET_FAIL"))
		{
			dialogBoldText.setText(R.string.failed);
			dialogText1.setText(R.string.password_reset_failed);
		}
		else if(dialogName.equals("ALARM_UPDATED"))
		{
			dialogBoldText.setText(R.string.alarm_updated);
			dialogText1.setText(R.string.alarm_updated_successfully);
		}
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.relOkDialog)
		{
			if((dialogName.equals("LOGIN_FAILED"))||(dialogName.equals("PASSWORDS_MISMATCH"))||
					(dialogName.equals("REGISTRATION_BLANK"))||(dialogName.equals("NO_VALID_EMAIL"))
					||(dialogName.equals("REGISTRATION_FAILED"))||(dialogName.equals("NO_INTERNET"))
					||(dialogName.equals("PASSWORD_RESET_FAIL"))||(dialogName.equals("ALARM_UPDATED")))
			{
				dismiss();
			}
			else if(dialogName.equals("REGISTERTRATION_SUCCESSFUL"))
			{

//				context.startActivity(new Intent(context, PlannerActivity.class));
//				((Activity) context).finish();

			}
			else if(dialogName.equals("PASSWORD_RESET"))
			{
				dismiss();
				((Activity) context).finish();
				
			}
					
			
		}
	}

}
