/*
 * Copyright (C) 2011 Daniel Berndt - Codeus Ltd  -  DateSlider
 *
 * This is a small demo application which demonstrates the use of the
 * dateSelector
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infogain.timesheet.DateSlider;

import java.util.Calendar;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.infogain.timesheet.R;
import com.infogain.timesheet.DateSlider.labeler.TimeLabeler;
import com.infogain.timesheet.Dialogs.LoginMesDialog;
import com.infogain.timesheet.Util.ProgressHUD;

/**
 * Small Demo activity which demonstrates the use of the DateSlideSelector
 *
 * @author Daniel Berndt - Codeus Ltd
 *
 */
public class Demo extends Activity {

static final int DEFAULTDATESELECTOR_ID = 0;
static final int DEFAULTDATESELECTOR_WITHLIMIT_ID = 6;
static final int ALTERNATIVEDATESELECTOR_ID = 1;
static final int CUSTOMDATESELECTOR_ID = 2;
static final int MONTHYEARDATESELECTOR_ID = 3;
static final int TIMESELECTOR_ID = 4;
static final int TIMESELECTOR_WITHLIMIT_ID = 7;
static final int DATETIMESELECTOR_ID = 5;

ProgressHUD mProgressHUD;

    private TextView dateText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // load and initialise the Activity
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datetimeslider_main);
        dateText = (TextView) this.findViewById(R.id.selectedDateLabel);

		mProgressHUD = ProgressHUD.show(Demo.this,"Please wait...", true,true,null);

        Button alternativeButton = (Button) this.findViewById(R.id.alternativeDateSelectButton);
        // set up a listener for when the button is pressed
        alternativeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                // call the internal showDialog method using the predefined ID
                showDialog(ALTERNATIVEDATESELECTOR_ID);
            }
        });



        Button timeButton = (Button) this.findViewById(R.id.timeSelectButton);
        // set up a listener for when the button is pressed
        timeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                // call the internal showDialog method using the predefined ID
                showDialog(TIMESELECTOR_ID);
            }
        });
        


        Button dateTimeButton = (Button) this.findViewById(R.id.dateTimeSelectButton);
        // set up a listener for when the button is pressed
        dateTimeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                // call the internal showDialog method using the predefined ID
              ///  showDialog(DATETIMESELECTOR_ID);
            	
            	LoginMesDialog dialog = new LoginMesDialog(Demo.this , "NO_INTERNET");
				dialog.setCancelable(false);
				dialog.show();
            }
        });
    }

    // define the listener which is called once a user selected the date.
    private DateSlider.OnDateSetListener mDateSetListener =
        new DateSlider.OnDateSetListener() {
            public void onDateSet(DateSlider view, Calendar selectedDate) {
                // update the dateText view with the corresponding date
                dateText.setText(String.format("Infogain Time chosen date:%n%te. %tB %tY", selectedDate, selectedDate, selectedDate));
            }
    };

    private DateSlider.OnDateSetListener mTimeSetListener =
        new DateSlider.OnDateSetListener() {
            public void onDateSet(DateSlider view, Calendar selectedDate) {
                // update the dateText view with the corresponding date
                dateText.setText(String.format("Infogain Time chosen time:%n%tR", selectedDate));
            }
    };

    private DateSlider.OnDateSetListener mDateTimeSetListener =
        new DateSlider.OnDateSetListener() {
            public void onDateSet(DateSlider view, Calendar selectedDate) {
                // update the dateText view with the corresponding date
                int minute = selectedDate.get(Calendar.MINUTE) /
                        TimeLabeler.MINUTEINTERVAL*TimeLabeler.MINUTEINTERVAL;
                dateText.setText(String.format("The chosen date and time:%n%te. %tB %tY%n%tH:%02d",
                        selectedDate, selectedDate, selectedDate, selectedDate, minute));
            }
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        // this method is called after invoking 'showDialog' for the first time
        // here we initiate the corresponding DateSlideSelector and return the dialog to its caller
    	
        final Calendar c = Calendar.getInstance();
        switch (id) {
        case ALTERNATIVEDATESELECTOR_ID:
        	final Calendar maxTimeYear = Calendar.getInstance();
        	maxTimeYear.add(Calendar.YEAR, 6);        	
        	final Calendar minTimeYear = Calendar.getInstance();
        	minTimeYear.add(Calendar.YEAR, -4);        	
            return new AlternativeDateSlider(this,mDateSetListener,c,minTimeYear,maxTimeYear);

        case TIMESELECTOR_ID:
            return new TimeSlider(this,mTimeSetListener,c,1);

        case DATETIMESELECTOR_ID:
            return new DateTimeSlider(this,mDateTimeSetListener,c);
        }
        return null;
    }
    
    
	private boolean isNetWorkAvailableInfo() {
		ConnectivityManager connectivity = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}
}