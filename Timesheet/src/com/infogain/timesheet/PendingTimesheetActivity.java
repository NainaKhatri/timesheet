package com.infogain.timesheet;

import java.util.Calendar;

import com.infogain.timesheet.DateSlider.AlternativeDateSlider;
import com.infogain.timesheet.DateSlider.DateSlider;
import com.infogain.timesheet.DateSlider.DateTimeSlider;
import com.infogain.timesheet.DateSlider.TimeSlider;
import com.infogain.timesheet.DateSlider.labeler.TimeLabeler;
import com.infogain.timesheet.adapter.DrawerAdapter;

import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.infogain.timesheet.R;

public class PendingTimesheetActivity extends ActionBarActivity implements
OnItemClickListener {
	DrawerLayout drawer;
	DrawerAdapter adapter;
	ListView drawerList;
	Button logoutButton;
	Button homeButton;
	ActionBarDrawerToggle actionBarDrawerToggle;
	FrameLayout fl_dateTime;
	TextView inprogressTimesheet2;
	static final int ALTERNATIVEDATESELECTOR_ID = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pending_timesheet);
		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		String[] drawerArray = getResources().getStringArray(
				R.array.drawer_array);
		adapter = new DrawerAdapter(this, drawerArray);

		drawerList = (ListView) findViewById(R.id.left_drawer);
		drawerList.setAdapter(adapter);
		drawerList.setOnItemClickListener(this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		actionBarDrawerToggle = new ActionBarDrawerToggle(
				this, 
				drawer,
				R.drawable.menu_icon,
				R.string.open_drawer,
				R.string.close_drawer) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}

			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
			}
		};

		drawer.setDrawerListener(actionBarDrawerToggle);

		fl_dateTime = (FrameLayout) findViewById(R.id.fl_dateTime);
		fl_dateTime.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		inprogressTimesheet2 = (TextView) findViewById(R.id.inprogressTimesheet2);
		inprogressTimesheet2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(ALTERNATIVEDATESELECTOR_ID);
			}
		});
		// logoutButton=(Button) findViewById(R.id.logoutButton);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pending_timesheet, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		int id = item.getItemId();
		//		if (id == R.id.action_settings) {
		//			return true;
		//		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		System.out.println("in on item click");
		openFragment(position);
	}

	private void openFragment(int position) {
		FragmentManager manager = getSupportFragmentManager();
		switch (position) {
		case 1:
			MyAreaFragment otherFragment = new MyAreaFragment();
			manager.beginTransaction()
			.replace(R.id.content_frame, otherFragment).commit();
			break;
			// MyAreaActivity firstFragment = new MyAreaActivity();
			// manager.beginTransaction().replace(R.id.content_frame,
			// firstFragment).commit();
			// break;
			// case 1:
			// FragmentTwo otherFragment = new FragmentTwo();
			// manager.beginTransaction()
			// .replace(R.id.content_frame, otherFragment).commit();
			// break;
			// case 2:
			// AppPrefsFragment prefsFragment = new AppPrefsFragment();
			// manager.beginTransaction()
			// .replace(R.id.content_frame, prefsFragment).commit();
			// break;
			//
		default:
			break;
		}
		drawer.closeDrawers();
		// drawer.closeDrawer(drawerList);

	}

	public void submitAction(View v) {
		Intent timesheetIntent = new Intent(this, AttendanceActivity.class);
		startActivity(timesheetIntent);
		drawer.closeDrawers();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		actionBarDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

	public void homeAction(View v) {
		drawer.closeDrawers();
	}

	public void logoutAction(View v) {
		System.out.println("in logout action");
		Intent loginIntent = new Intent(this, LoginActivity.class);
		startActivity(loginIntent);
		//drawer.closeDrawers();
	}

	private DateSlider.OnDateSetListener mDateSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			// inprogressTimesheet2.setText(String.format("Infogain Time chosen date:%n%te. %tB %tY",
			// selectedDate, selectedDate, selectedDate));
			inprogressTimesheet2.setText(String.format("%te. %tB %tY",
					selectedDate, selectedDate, selectedDate));
		}
	};

	private DateSlider.OnDateSetListener mTimeSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			inprogressTimesheet2.setText(String.format(
					"Infogain Time chosen time:%n%tR", selectedDate));
		}
	};

	private DateSlider.OnDateSetListener mDateTimeSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			int minute = selectedDate.get(Calendar.MINUTE)
					/ TimeLabeler.MINUTEINTERVAL * TimeLabeler.MINUTEINTERVAL;
			inprogressTimesheet2.setText(String.format(
					"The chosen date and time:%n%te. %tB %tY%n%tH:%02d",
					selectedDate, selectedDate, selectedDate, selectedDate,
					minute));
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		// this method is called after invoking 'showDialog' for the first time
		// here we initiate the corresponding DateSlideSelector and return the
		// dialog to its caller

		final Calendar c = Calendar.getInstance();
		switch (id) {
		case ALTERNATIVEDATESELECTOR_ID:
			final Calendar maxTimeYear = Calendar.getInstance();
			maxTimeYear.add(Calendar.YEAR, 6);
			final Calendar minTimeYear = Calendar.getInstance();
			minTimeYear.add(Calendar.YEAR, -4);
			return new AlternativeDateSlider(this, mDateSetListener, c,
					minTimeYear, maxTimeYear);

			// case TIMESELECTOR_ID:
			// return new TimeSlider(this,mTimeSetListener,c,1);
			//
			// case DATETIMESELECTOR_ID:
			// return new DateTimeSlider(this,mDateTimeSetListener,c);
		}
		return null;
	}

	private boolean isNetWorkAvailableInfo() {
		ConnectivityManager connectivity = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}

}
