package com.infogain.timesheet.timesheet_task_screen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.infogain.timesheet.R;
import com.infogain.timesheet.DateSlider.DateSlider;
import com.infogain.timesheet.DateSlider.TimeSlider;
import com.infogain.timesheet.Util.ProgressHUD;

public class TimesheetTaskScreen extends Activity {
	private String mText;
	Spinner projectSpinner, locationSpinner, taskSpinner, subtaskSpinner,
	hrsSpinner;

	RelativeLayout rl_week1, rl_week2, rl_week3, rl_week4, rl_week5, rl_week6, rl_week7;
	TextView txt_week1_time,txt_week2_time,txt_week3_time, txt_week4_time, txt_week5_time, txt_week6_time, txt_week7_time;
	public static String[] spinnerValue = { "Ultimate Game", "Need for Speed", "Ulimate Racing", "Rockstar Games", "Thunder Bolt" }; 
	long totalHOURSWeek1 = 0l;
	long totalHOURSWeek2  = 0l;
	long totalHOURSWeek3  = 0l;
	long totalHOURSWeek4  = 0l;
	long totalHOURSWeek5  = 0l;
	long totalHOURSWeek6  = 0l;
	long totalHOURSWeek7  = 0l;

	long totalMINUTESWeek1 = 0l;
	long totalMINUTESWeek2  = 0l;
	long totalMINUTESWeek3  = 0l;
	long totalMINUTESWeek4  = 0l;
	long totalMINUTESWeek5  = 0l;
	long totalMINUTESWeek6  = 0l;
	long totalMINUTESWeek7  = 0l;

	TextView totalHoursTXT;


	int totalHoursADDED;
	int totalMinutesADDED;


	ProgressHUD mProgressHUD;
	static final int ALTERNATIVEDATESELECTOR_ID = 1;
	static final int TIMESELECTOR_ID = 2;
	boolean week1Selected = false;
	boolean week2Selected = false;
	boolean week3Selected = false;
	boolean week4Selected = false;
	boolean week5Selected = false;
	boolean week6Selected = false;
	boolean week7Selected = false;
	Calendar cal;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.timesheet_task_screen);

		cal = GregorianCalendar.getInstance();

		projectSpinner = (Spinner) findViewById(R.id.project_spinner);
		locationSpinner = (Spinner) findViewById(R.id.location_spinner);
		taskSpinner = (Spinner) findViewById(R.id.task_spinner);
		subtaskSpinner = (Spinner) findViewById(R.id.subtask_spinner);
		hrsSpinner = (Spinner) findViewById(R.id.hrs_spinner);

		ArrayAdapter<CharSequence> projectAdapter = ArrayAdapter
				.createFromResource(this, R.array.project_array,
						android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> locationAdapter = ArrayAdapter
				.createFromResource(this, R.array.location_array,
						android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> hrsAdapter = ArrayAdapter
				.createFromResource(this, R.array.hrs_array,
						android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> taskAdapter = ArrayAdapter
				.createFromResource(this, R.array.task_array,
						android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> subtaskAdapter = ArrayAdapter
				.createFromResource(this, R.array.subtask_array,
						android.R.layout.simple_spinner_item);

		projectAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		locationAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		hrsAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		taskAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		subtaskAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		projectSpinner.setAdapter(projectAdapter);
		locationSpinner.setAdapter(locationAdapter);
		hrsSpinner.setAdapter(hrsAdapter);

		taskSpinner.setAdapter(taskAdapter);
		subtaskSpinner.setAdapter(subtaskAdapter);

		//		new HttpAsyncTaskTimeSheet()
		//		.execute("http://172.18.87.32:54155/api/LoginAPI/");

		clickWeekEvent();
		//fillSpinnerValues();

	}



	public void clickWeekEvent()
	{
		rl_week1 = (RelativeLayout)findViewById(R.id.rl_week1);
		rl_week1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				week1Selected = true;
				showDialog(TIMESELECTOR_ID);
			}
		});

		txt_week1_time = (TextView)findViewById(R.id.txt_week1_time);
		rl_week2 = (RelativeLayout)findViewById(R.id.rl_week2);
		rl_week2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				week2Selected = true;
				showDialog(TIMESELECTOR_ID);
			}
		});
		txt_week2_time = (TextView)findViewById(R.id.txt_week2_time);

		rl_week3 = (RelativeLayout)findViewById(R.id.rl_week3);
		rl_week3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				week3Selected = true;
				showDialog(TIMESELECTOR_ID);
			}
		});
		txt_week3_time = (TextView)findViewById(R.id.txt_week3_time);

		rl_week4 = (RelativeLayout)findViewById(R.id.rl_week4);
		rl_week4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				week4Selected = true;
				showDialog(TIMESELECTOR_ID);
			}
		});
		txt_week4_time = (TextView)findViewById(R.id.txt_week4_time);

		rl_week5 = (RelativeLayout)findViewById(R.id.rl_week5);
		rl_week5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				week5Selected = true;
				showDialog(TIMESELECTOR_ID);
			}
		});
		txt_week5_time = (TextView)findViewById(R.id.txt_week5_time);

		rl_week6 = (RelativeLayout)findViewById(R.id.rl_week6);
		rl_week6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				week6Selected = true;
				showDialog(TIMESELECTOR_ID);
			}
		});
		txt_week6_time = (TextView)findViewById(R.id.txt_week6_time);

		rl_week7 = (RelativeLayout)findViewById(R.id.rl_week7);
		rl_week7.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				week7Selected = true;
				showDialog(TIMESELECTOR_ID);
			}
		});
		txt_week7_time = (TextView)findViewById(R.id.txt_week7_time);

		totalHoursTXT = (TextView)findViewById(R.id.ttlhrs);
	}

	public class HttpAsyncTaskTimeSheet extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			mProgressHUD = ProgressHUD.show(TimesheetTaskScreen.this,
					"Loading...", true, false, null);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... urls) {
			// TODO Auto-generated method stub
			return POSTForFetchingDATA(urls[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			mProgressHUD.dismiss();

		}
	}

	private String POSTForFetchingDATA(String url) {
		// Making JSON format query

		InputStream inputStream = null;
		String result = "";
		try {

			// 1. create HttpClient
			HttpClient httpclient = new DefaultHttpClient();

			// 2. make POST request to the given URL
			HttpPost httpPost = new HttpPost(url.replaceAll(" ", ""));

			String json = "";

			// 3. build jsonObject
			JSONObject jsonObject = new JSONObject();
			// alarmDataToDelete
			jsonObject.accumulate("DomainId", "igglobal");
			jsonObject.accumulate("UserName", "divyanshu.srivastava");
			jsonObject.accumulate("Password", "@123!");

			// 4. convert JSONObject to JSON to String
			json = jsonObject.toString();

			// ** Alternative way to convert Person object to JSON string usin
			// Jackson Lib
			// ObjectMapper mapper = new ObjectMapper();
			// json = mapper.writeValueAsString(person);

			// 5. set json to StringEntity
			StringEntity se = new StringEntity(json);

			// 6. set httpPost Entity
			httpPost.setEntity(se);

			// 7. Set some headers to inform server about the type of the
			// content
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			// 8. Execute POST request to the given URL
			HttpResponse httpResponse = httpclient.execute(httpPost);

			// 9. receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// 10. convert inputstream to string
			if (inputStream != null)
				result = convertInputStreamToStringDeleteAlarm(inputStream);
			else
				result = "Did not work!";

		} catch (Exception e) {

			e.printStackTrace();

		}

		// 11. return result
		return result;
	}

	private String convertInputStreamToStringDeleteAlarm(InputStream inputStream)
			throws IOException {
		// TODO Auto-generated method stub
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		getJsonDataFromResult(result);
		System.out.println("R E S U L T :::::::" + result);
		return result;
	}

	private void getJsonDataFromResult(String result) {
		try {
			JSONObject object = new JSONObject(result);
			if (object.getString("Message").equalsIgnoreCase("Failed")) {
				System.out.println("MESSAGE   FAILED:::::");
			} else {
				System.out.println("MESSAGE   PASS:::::");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	private DateSlider.OnDateSetListener mDateSetListener =
			new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			//inprogressTimesheet2.setText(String.format("Infogain Time chosen date:%n%te. %tB %tY", selectedDate, selectedDate, selectedDate));
			//inprogressTimesheet2.setText(String.format("%te. %tB %tY", selectedDate, selectedDate, selectedDate));
		}
	};

	private DateSlider.OnDateSetListener mTimeSetListener =
			new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			if(week1Selected == true){
				txt_week1_time.setText(String.format("%tR", selectedDate).toString());
				System.out.println("T   I   M   E  ::::::"+selectedDate.HOUR +"  "+selectedDate.MINUTE);
				week1Selected =false;
			}

			else if(week2Selected == true){
				txt_week2_time.setText(String.format("%tR", selectedDate).toString());
				week2Selected =false;
			}

			else if(week3Selected == true){
				txt_week3_time.setText(String.format("%tR", selectedDate).toString());
				week3Selected =false;
			}
			else if(week4Selected == true){
				txt_week4_time.setText(String.format("%tR", selectedDate).toString());
				week4Selected =false;
			}
			else if(week5Selected == true){
				txt_week5_time.setText(String.format("%tR", selectedDate).toString());
				week5Selected =false;
			}
			else if(week6Selected == true){
				txt_week6_time.setText(String.format("%tR", selectedDate).toString());
				week6Selected =false;
			}
			else if(week7Selected == true){
				txt_week7_time.setText(String.format("%tR", selectedDate).toString());
				week7Selected =false;
			}		
			refreshTotalHours();
		}
	};

	public void calculateHours()
	{

		String hour1 = splitHours(txt_week1_time.getText().toString());
		String minutes1 = splitMinutes(txt_week1_time.getText().toString());
		totalHoursADDED = totalHoursADDED + Integer.parseInt(hour1);
		totalMinutesADDED = totalHoursADDED + Integer.parseInt(minutes1);

		totalHOURSWeek1 = Integer.parseInt(hour1);
		totalMINUTESWeek1 = Integer.parseInt(minutes1);



		String hour2 = splitHours(txt_week2_time.getText().toString());
		String minutes2 = splitMinutes(txt_week2_time.getText().toString());
		totalHoursADDED = totalHoursADDED + Integer.parseInt(hour2);
		totalMinutesADDED = totalHoursADDED + Integer.parseInt(minutes2);
		totalHOURSWeek2 = Integer.parseInt(hour2);
		totalMINUTESWeek2 = Integer.parseInt(minutes2);

		String hour3 = splitHours(txt_week3_time.getText().toString());
		String minutes3 = splitMinutes(txt_week3_time.getText().toString());
		totalHoursADDED = totalHoursADDED + Integer.parseInt(hour3);
		totalMinutesADDED = totalHoursADDED + Integer.parseInt(minutes3);
		totalHOURSWeek3 = Integer.parseInt(hour3);
		totalMINUTESWeek3 = Integer.parseInt(minutes3);


		String hour4 = splitHours(txt_week4_time.getText().toString());
		String minutes4 = splitMinutes(txt_week4_time.getText().toString());
		totalHoursADDED = totalHoursADDED + Integer.parseInt(hour4);
		totalMinutesADDED = totalHoursADDED + Integer.parseInt(minutes4);
		totalHOURSWeek4 = Integer.parseInt(hour4);
		totalMINUTESWeek4 = Integer.parseInt(minutes4);

		String hour5 = splitHours(txt_week5_time.getText().toString());
		String minutes5 = splitMinutes(txt_week5_time.getText().toString());
		totalHoursADDED = totalHoursADDED + Integer.parseInt(hour5);
		totalMinutesADDED = totalHoursADDED + Integer.parseInt(minutes5);
		totalHOURSWeek5 = Integer.parseInt(hour5);
		totalMINUTESWeek5 = Integer.parseInt(minutes5);

		String hour6 = splitHours(txt_week6_time.getText().toString());
		String minutes6 = splitMinutes(txt_week6_time.getText().toString());
		totalHoursADDED = totalHoursADDED + Integer.parseInt(hour6);
		totalMinutesADDED = totalHoursADDED + Integer.parseInt(minutes6);
		totalHOURSWeek6 = Integer.parseInt(hour6);
		totalMINUTESWeek6 = Integer.parseInt(minutes6);

		String hour7 = splitHours(txt_week7_time.getText().toString());
		String minutes7 = splitMinutes(txt_week7_time.getText().toString());
		totalHoursADDED = totalHoursADDED + Integer.parseInt(hour7);
		totalMinutesADDED = totalHoursADDED + Integer.parseInt(minutes7);
		totalHOURSWeek7 = Integer.parseInt(hour7);
		totalMINUTESWeek7 = Integer.parseInt(minutes7);

	}

	public String splitHours(String s)
	{			
		s = s.substring(s.indexOf(":")-2,s.indexOf(":"));	
		return s;
	}

	public String splitMinutes(String s)
	{			
		s = s.substring(s.indexOf(":")+1,s.indexOf(":")+3);		

		return s;
	}

	public void refreshTotalHours(){
		calculateHours();
		//int totalHoursCalculated = totalHoursADDED;
		long totalHoursCalculated =
				totalHOURSWeek1 + 
				totalHOURSWeek2 +
				totalHOURSWeek3 + 
				totalHOURSWeek4 + 
				totalHOURSWeek5 + 
				totalHOURSWeek6 + 
				totalHOURSWeek7;

		long minutesConvertedVal;
		//int totalMinutesCalculated =totalMinutesADDED;
		long totalMinutesCalculated =
				totalMINUTESWeek1 + 
				totalMINUTESWeek2 +
				totalMINUTESWeek3 + 
				totalMINUTESWeek4 +
				totalMINUTESWeek5 + 
				totalMINUTESWeek6 +
				totalMINUTESWeek7;

		if(totalMinutesCalculated>59)
		{
			minutesConvertedVal = totalMinutesCalculated /60;
			totalHoursCalculated = totalHoursCalculated + minutesConvertedVal;
			totalMinutesCalculated = totalMinutesCalculated %60;
		}
		String time = totalHoursCalculated+":"+totalMinutesCalculated;
		totalHoursTXT.setText(time);
	}

		@Override
	protected Dialog onCreateDialog(int id) {
		// this method is called after invoking 'showDialog' for the first time
		// here we initiate the corresponding DateSlideSelector and return the dialog to its caller

		final Calendar c = Calendar.getInstance();
		switch (id) {
	
		case TIMESELECTOR_ID:
			return new TimeSlider(this,mTimeSetListener,c,1);		
		}
		return null;
	}


	private boolean isNetWorkAvailableInfo() {
		ConnectivityManager connectivity = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}


	public class MyAdapter extends ArrayAdapter<String> { 

		public MyAdapter(Context ctx, int txtViewResourceId, String[] objects) 
		{ 
			super(ctx, txtViewResourceId, objects); 
			//System.out.println("P O S I T I O N:::::3::::");

		} 
		@Override public View getDropDownView(int position, View cnvtView, ViewGroup prnt) 
		{ 
			//System.out.println("P O S I T I O N:::::2::::");
			return getCustomView(position, cnvtView, prnt); 
		} 
		@Override public View getView(int pos, View cnvtView, ViewGroup prnt) 
		{ 
			//System.out.println("P O S I T I O N:::::1::::");

			return getCustomView(pos, cnvtView, prnt); 

		} 

		public View getCustomView(int position, View convertView, ViewGroup parent)
		{
			LayoutInflater inflater =getLayoutInflater(); 
			View mySpinner = inflater.inflate(R.layout.custom_spinner, parent, false); 
			TextView main_text = (TextView) mySpinner .findViewById(R.id.text_main_seen);
			main_text.setText(TimesheetTaskScreen.spinnerValue[position]); 
			main_text.setTextColor(getResources().getColor(R.color.app_blue_color));

			return mySpinner; 
		} 
	}

	public void fillSpinnerValues()
	{

		Spinner mySpinner = (Spinner) findViewById(R.id.project_spinner); 
		mySpinner.setAdapter(new MyAdapter(TimesheetTaskScreen.this, R.layout.custom_spinner, spinnerValue));
		mySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});



	}



}
