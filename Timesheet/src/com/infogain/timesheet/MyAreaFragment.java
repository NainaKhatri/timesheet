package com.infogain.timesheet;

import com.infogain.timesheet.R;
import com.infogain.timesheet.adapter.TabsViewPagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class MyAreaFragment extends Fragment implements ActionBar.TabListener{
	TabsViewPagerAdapter adapter;
	ViewPager viewPager;
	ActionBar actionBar;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.activity_my_area,
				container, false);
//		RelativeLayout relLayout = (RelativeLayout) rootView
//				.findViewById(R.id.cardLayout);
		actionBar=getActivity().getActionBar();
		adapter = new TabsViewPagerAdapter(getFragmentManager());
		viewPager = (ViewPager) rootView.findViewById(R.id.pager);
		viewPager.setAdapter(adapter);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		String[] tabs=getResources().getStringArray(R.array.reports_array);
		
		 for (String tab_name : tabs) {
	            actionBar.addTab(actionBar.newTab().setText(tab_name)
	                    .setTabListener(this));
	        }
		
		//actionBar.setCustomView(R.color.primary_color);
	
		// Adding Tabs
//        for (String tab_name : tabs) {
//            actionBar.addTab(actionBar.newTab().setText(tab_name)
//                    .setTabListener(this));
//        }

		
		// ListView listView = (ListView) rootView.findViewById(R.id.listView);
		// WeeklySummaryAdapter adapter=new
		// WeeklySummaryAdapter(3,getActivity());
		// listView.setAdapter(adapter);
		return rootView;
	}

	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_my_area);
//
//		actionBar = getSupportActionBar();
//		adapter = new TabsViewPagerAdapter(getSupportFragmentManager());
//		viewPager = (ViewPager) findViewById(R.id.pager);
//		viewPager.setAdapter(adapter);
//		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//		
//		String[] tabs=getResources().getStringArray(R.array.reports_array);
//		
//		 for (String tab_name : tabs) {
//	            actionBar.addTab(actionBar.newTab().setText(tab_name)
//	                    .setTabListener(this));
//	        }
//		
//		//actionBar.setCustomView(R.color.primary_color);
//	
//		// Adding Tabs
////        for (String tab_name : tabs) {
////            actionBar.addTab(actionBar.newTab().setText(tab_name)
////                    .setTabListener(this));
////        }
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.my_area, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
//
//	@Override
//	public void onTabSelected(Tab tab, FragmentTransaction ft) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void onTabReselected(Tab tab, FragmentTransaction ft) {
//		// TODO Auto-generated method stub
//		
//	}



}
