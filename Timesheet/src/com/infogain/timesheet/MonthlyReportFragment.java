package com.infogain.timesheet;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class MonthlyReportFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.monthly_report_fragment,
				container, false);
		RelativeLayout relLayout = (RelativeLayout) rootView
				.findViewById(R.id.cardLayout);

		int count = 2;
		for (int i = 0; i < count; i++) {
			LayoutInflater reportInflater = (LayoutInflater) getActivity()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View reportView = reportInflater.inflate(R.layout.weekly_report_cardview,
					null);
			relLayout.addView(reportView);
		}
		// ListView listView = (ListView) rootView.findViewById(R.id.listView);
		// WeeklySummaryAdapter adapter=new
		// WeeklySummaryAdapter(3,getActivity());
		// listView.setAdapter(adapter);
		return rootView;
	}

	// public void onAttach(Activity activity) {
	// LayoutInflater viewInflater = (LayoutInflater) activity
	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// View listView = viewInflater.inflate(R.layout.report_listview, null);
	// System.out.println("rootView=" + listView);
	//
	// }
}
