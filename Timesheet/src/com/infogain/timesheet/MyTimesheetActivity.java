package com.infogain.timesheet;

import com.infogain.timesheet.R;
import com.infogain.timesheet.adapter.TabsViewPagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MyTimesheetActivity extends ActionBarActivity {

	TabsViewPagerAdapter adapter;
	ViewPager viewPager;
	ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_timesheet);

		actionBar = getSupportActionBar();
		// adapter = new TabsViewPagerAdapter(getSupportFragmentManager());
	//	viewPager = (ViewPager) findViewById(R.id.pager);
		// viewPager.setAdapter(adapter);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		final int tabCount = actionBar.getTabCount();
		final String text = "Project " + (tabCount + 1);

		Fragment projectFragment = new ProjectTabFragment(text);
		Fragment weeklyFragment=new WeeklyReportFragment();
		actionBar.addTab(actionBar.newTab().setText(text)
				.setTabListener(new TabListener(projectFragment)));
		
		actionBar.addTab(actionBar.newTab().setText(text)
				.setTabListener(new TabListener(weeklyFragment)));
				//.setTabListener(new TabListener(projectFragment)));

		// .setTabListener(new TabListener(
			//	.setTabListener(new TabListener(projectFragment)));

		// String[] tabs=getResources().getStringArray(R.array.reports_array);
		// actionBar.setCustomView(R.color.primary_color);

		// Adding Tabs
		// for (String tab_name : tabs) {
		// actionBar.addTab(actionBar.newTab().setText(tab_name)
		// .setTabListener(this));
		// }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_timesheet, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class TabListener implements ActionBar.TabListener {

		private Fragment mFragment;

		public TabListener(Fragment fragment) {
			mFragment = fragment;
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			// TODO Auto-generated method stub
		
			ft.add(R.id.fragment_container, mFragment);
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// TODO Auto-generated method stub
			
			if(tab.getPosition()==0)
				mFragment=new ProjectTabFragment();
			else 
				mFragment=new WeeklyReportFragment();
			ft.replace(R.id.fragment_container, mFragment);
		//	ft.addToBackStack(null);
			ft.commit();

		}

	}

}
