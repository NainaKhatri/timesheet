package com.infogain.timesheet;

/*
 * 
 * 
 * Author: Naina Khatri
 * 
 */
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.infogain.timesheet.DateSlider.Demo;
import com.infogain.timesheet.adapter.DrawerAdapter;
import com.infogain.timesheet.adapter.TaskAdapter;
import com.infogain.timesheet.timesheet_task_screen.TimesheetTaskScreen;

public class AttendanceActivity extends ActionBarActivity implements
OnItemClickListener{

	Button submitButton;
	Button saveButton;
	ListView taskList;


	DrawerLayout drawer;
	DrawerAdapter adapter;
	ListView drawerList;
	ActionBarDrawerToggle actionBarDrawerToggle;


	// test
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attendance);
		//activateActionBar();
		

		drawer = (DrawerLayout) findViewById(R.id.drawer_layout_dl);
		String[] drawerArray = getResources().getStringArray(
				R.array.drawer_array);
		adapter = new DrawerAdapter(this, drawerArray);

		drawerList = (ListView) findViewById(R.id.left_drawer);
		drawerList.setAdapter(adapter);
		drawerList.setOnItemClickListener(this);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		actionBarDrawerToggle = new ActionBarDrawerToggle(
				this, 
				drawer,
				R.drawable.menu_icon,
				R.string.open_drawer,
				R.string.close_drawer) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}

			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
			}
		};

		drawer.setDrawerListener(actionBarDrawerToggle);
	
		submitButton = (Button) findViewById(R.id.submitButton);
		submitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent dialog = new Intent(AttendanceActivity.this, Demo.class);
				startActivity(dialog);
			}
		});

		taskList = (ListView) findViewById(R.id.taskList);
		TaskAdapter taskAdapter=new TaskAdapter(this,null);
		taskList.setAdapter(taskAdapter);

		saveButton = (Button) findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent dialog = new Intent(AttendanceActivity.this,
						LeaveApprovalPopupActivity.class);
				startActivity(dialog);
			}
		});

	}

	
	
	public void activateActionBar()
	{
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.attendance, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		int id = item.getItemId();
		if (id == R.id.my_area) {
			// MyAreaActivity areaActivity=MyAreaActivity();
			Intent i = new Intent(this, MyAreaActivity.class);
			startActivity(i);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void addTaskAction(View v) {
		Intent timesheetIntent=new Intent(this,TimesheetTaskScreen.class);
		startActivity(timesheetIntent);
	}

	public void leaveAction(View v) {
		System.out.println("in leave action");
		Intent i = new Intent(this, LeaveApprovalPopupActivity.class);
		startActivity(i);

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		System.out.println("in on item click");
		openFragment(position);
	}

	private void openFragment(int position) {
		FragmentManager manager = getSupportFragmentManager();
		switch (position) {
		case 1:
			MyAreaFragment otherFragment = new MyAreaFragment();
			manager.beginTransaction()
			.replace(R.id.content_frame, otherFragment).commit();
			break;
			// MyAreaActivity firstFragment = new MyAreaActivity();
			// manager.beginTransaction().replace(R.id.content_frame,
			// firstFragment).commit();
			// break;
			// case 1:
			// FragmentTwo otherFragment = new FragmentTwo();
			// manager.beginTransaction()
			// .replace(R.id.content_frame, otherFragment).commit();
			// break;
			// case 2:
			// AppPrefsFragment prefsFragment = new AppPrefsFragment();
			// manager.beginTransaction()
			// .replace(R.id.content_frame, prefsFragment).commit();
			// break;
			//
		default:
			break;
		}
		drawer.closeDrawers();
		// drawer.closeDrawer(drawerList);

	}

	public void submitAction(View v) {
		Intent timesheetIntent = new Intent(this, AttendanceActivity.class);
		startActivity(timesheetIntent);
		drawer.closeDrawers();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		actionBarDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		actionBarDrawerToggle.onConfigurationChanged(newConfig);
	}

	public void homeAction(View v) {
		drawer.closeDrawers();
	}

	public void logoutAction(View v) {
		System.out.println("in logout action");
		Intent loginIntent = new Intent(this, LoginActivity.class);
		startActivity(loginIntent);
		//drawer.closeDrawers();
	}
}
