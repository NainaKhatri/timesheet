package com.infogain.timesheet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class ProjectTabFragment extends Fragment {
	private String mText;

	public ProjectTabFragment(String text) {
		mText = text;
	}

	public ProjectTabFragment() {
	}

	public String getText() {
		return mText;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Spinner projectSpinner, locationSpinner, taskSpinner, subtaskSpinner, hrsSpinner;
		View fragView = inflater.inflate(R.layout.timesheet_task_screen, container,
				false);
		projectSpinner = (Spinner) fragView.findViewById(R.id.project_spinner);
		locationSpinner = (Spinner) fragView
				.findViewById(R.id.location_spinner);
		taskSpinner = (Spinner) fragView.findViewById(R.id.task_spinner);
		subtaskSpinner = (Spinner) fragView.findViewById(R.id.subtask_spinner);
		hrsSpinner = (Spinner) fragView.findViewById(R.id.hrs_spinner);

		ArrayAdapter<CharSequence> projectAdapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.project_array,
				android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> locationAdapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.location_array,
				android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> hrsAdapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.hrs_array,
				android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> taskAdapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.task_array,
				android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> subtaskAdapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.subtask_array,
				android.R.layout.simple_spinner_item);

		projectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		locationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		hrsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		taskAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		subtaskAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		
		
		projectSpinner.setAdapter(projectAdapter);
		locationSpinner.setAdapter(locationAdapter);
		hrsSpinner.setAdapter(hrsAdapter);
		
		taskSpinner.setAdapter(taskAdapter);
		subtaskSpinner.setAdapter(subtaskAdapter);
		

		return fragView;
	}
	
	
	public void saveAction(View view)
	{
		FragmentTransaction ft=getActivity().getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.fragment_container,new WeeklyReportFragment());
	}
}
