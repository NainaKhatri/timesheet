package com.infogain.timesheet.UIControls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class AutoCompleteEditTextUbuntuRegular extends AutoCompleteTextView{

	public AutoCompleteEditTextUbuntuRegular(Context context,
			AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public AutoCompleteEditTextUbuntuRegular(Context context,
			AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public AutoCompleteEditTextUbuntuRegular(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	private void init(Context context) {
		Typeface face = Typeface.createFromAsset(context.getAssets(), "font/UbuntuRegularFont.ttf");
		setTypeface(face );		
	}
}
