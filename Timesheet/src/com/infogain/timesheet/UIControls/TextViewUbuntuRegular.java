package com.infogain.timesheet.UIControls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewUbuntuRegular extends TextView {

	public TextViewUbuntuRegular(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public TextViewUbuntuRegular(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public TextViewUbuntuRegular(Context context) {
		super(context);
		init(context);
	}

	private void init(Context context) {
		Typeface face = Typeface.createFromAsset(context.getAssets(), "font/UbuntuRegularFont.ttf");
		setTypeface(face);		
	}
}
