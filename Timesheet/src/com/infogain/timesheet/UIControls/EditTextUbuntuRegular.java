package com.infogain.timesheet.UIControls;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextUbuntuRegular extends EditText {

	
	public EditTextUbuntuRegular(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	public EditTextUbuntuRegular(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	public EditTextUbuntuRegular(Context context) {
		super(context);
		init(context);
	}
	
	

	private void init(Context context) {
		Typeface face = Typeface.createFromAsset(context.getAssets(), "font/UbuntuRegularFont.ttf");
		setTypeface(face );		
	}

}
