package com.infogain.timesheet.adapter;

import com.infogain.timesheet.R;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerAdapter extends BaseAdapter {

	Activity activity;
	String[] drawerArray;

	public DrawerAdapter(Activity activity, String[] array) {
		this.activity = activity;
		this.drawerArray = array;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return drawerArray.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rowview;
		rowview = convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowview = inflater.inflate(R.layout.residemenu_item, parent,
					false);
			if(position==(drawerArray.length-1))
			{
				System.out.println("invisible");
				View view=rowview.findViewById(R.id.viewSeparator);
				view.setVisibility(View.INVISIBLE);
			}
			// TextView textView = (TextView) rowView.findViewById(R.id.label);
		}
		
		ImageView iv_icon = (ImageView) rowview.findViewById(R.id.iv_icon);
		TextView tv_title = (TextView) rowview.findViewById(R.id.tv_title);
	
		String name="drawer_image"+(position+1);
		//String[] name=draname===werArray[position].split(" ");
		int id=activity.getResources().getIdentifier(name,
                "drawable", activity.getPackageName());
		
		iv_icon.setImageResource(id);
		
		tv_title.setText(drawerArray[position]);
		
		return rowview;
	}
}
