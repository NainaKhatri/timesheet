package com.infogain.timesheet.adapter;

import com.infogain.timesheet.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class WeeklySummaryAdapter extends BaseAdapter {
	int count;
	Activity activity;

	public WeeklySummaryAdapter(int count, Activity activity) {
		this.count = count;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return count;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		System.out.println("in get view");
		View rowview;
		rowview=convertView;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 rowview = inflater.inflate(R.layout.weekly_report_cardview, parent, false);
			//TextView textView = (TextView) rowView.findViewById(R.id.label);
		}
		return rowview;
	}

}
