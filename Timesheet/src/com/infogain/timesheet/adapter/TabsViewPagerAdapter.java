package com.infogain.timesheet.adapter;

import com.infogain.timesheet.WeeklyReportFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsViewPagerAdapter extends FragmentPagerAdapter {
 
    public TabsViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }
 
    @Override
    public Fragment getItem(int index) {
 
        switch (index) {
        case 0:
            // Top Rated fragment activity
           // return new TopRatedFragment();
            return new WeeklyReportFragment();
        case 1:
            // Games fragment activity
       //     return new GamesFragment();
            return new WeeklyReportFragment();
        }
 
        return null;
    }
 
    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 2;
    }
}